{
"trial_manager" : "Gestor de casos",

"own_trials" : "Tus casos",
"collaborating_trials" : "Casos en los que colaboras",

"independant_trials" : "Casos independientes",
"trial_sequences" : "Series de casos",

"open_editor" : "Abrir en el editor",
"save" : "Guardar",
"confirm_delete" : "Seguro que quiere borrar ésto elemento? Ésta acción es irreversible.",

"case_rules" : "Usted ha leído las <a href=\"<url>\">sobre los casos</a>, y este caso estará al margen de esta."
}
