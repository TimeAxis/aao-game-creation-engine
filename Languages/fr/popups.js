{
"default_popups_fouet" : "Objets des personnages|Fouet",
"default_popups_fouet-inversee" : "Objets des personnages|Fouet (Inversé)",

"default_popups_coupable" : "Evénements|Coupable",
"default_popups_contre-interrogatoire" : "Evénements|Contre-interrogatoire",
"default_popups_non-coupable" : "Evénements|Non Coupable",
"default_popups_deposition-du-temoin" : "Evénements|Déposition du témoin",
"default_popups_game-over-doors" : "Evénements|Game Over doors",

"default_popups_eureka" : "Bulles|Eureka !",
"default_popups_j-te-tiens" : "Bulles|J'te tiens !",
"default_popups_un-instant" : "Bulles|Un Instant !",
"default_popups_prends-ca" : "Bulles|Prends ça !",
"default_popups_objection" : "Bulles|Objection !",

"default_popups_guilty" : "Graphismes anglais|Coupable",
"default_popups_cross-examination" : "Graphismes anglais|Contre-interrogatoire",
"default_popups_not-guilty" : "Graphismes anglais|Non Coupable",
"default_popups_witness-testimony" : "Graphismes anglais|Déposition du témoin",
"default_popups_testimony-statement" : "Graphismes anglais|Déclaration du témoin",
"default_popups_unlock-successful" : "Graphismes anglais|Unlock successful",
"default_popups_gotcha" : "Graphismes anglais|J'te tiens !",
"default_popups_hold-it" : "Graphismes anglais|Un Instant !",
"default_popups_not-so-fast" : "Graphismes anglais|Not so fast!",
"default_popups_take-that" : "Graphismes anglais|Prends ça !",

"default_popups_aussage" : "Graphismes allemands|Déclaration du témoin",
"default_popups_einspruch" : "Graphismes allemands|Objection !",
"default_popups_ertappt" : "Graphismes allemands|J'te tiens !",
"default_popups_kreuzverhor" : "Graphismes allemands|Contre-interrogatoire",
"default_popups_moment-mal" : "Graphismes allemands|Un Instant !",
"default_popups_nimm-das" : "Graphismes allemands|Prends ça !",
"default_popups_schuldig" : "Graphismes allemands|Coupable",
"default_popups_zeugenaussage" : "Graphismes allemands|Déposition du témoin",

"default_popups_te-tengo" : "Graphismes espagnols|J'te tiens !",
"default_popups_un-momento" : "Graphismes espagnols|Un Instant !",
"default_popups_protesto" : "Graphismes espagnols|Objection!",
"default_popups_toma-ya" : "Graphismes espagnols|Prends ça !"
}
